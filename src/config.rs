#[derive(Debug)]
pub struct Config {
    // Where the content lives
    pub content_folder: String,
    // Where to write the generated files
    pub output_folder: String,
    pub enable_tables: bool,
    pub enable_footnotes: bool,
    pub enable_strikethrough: bool,
    pub enable_tasklists: bool,
    pub enable_smart_punctuation: bool,
    pub enable_heading_attributes: bool
}

impl Default for Config {
    fn default() -> Self {
        Self { 
            content_folder: Default::default(), 
            output_folder: Default::default(), 
            enable_tables: true, 
            enable_footnotes: true, 
            enable_strikethrough: true, 
            enable_tasklists: true, 
            enable_smart_punctuation: true, 
            enable_heading_attributes: true
        }
    }
}

impl Config {
    pub fn to_pulldown_opts(&self) -> pulldown_cmark::Options {
        let mut opts = pulldown_cmark::Options::empty();
        if self.enable_tables {
            opts.toggle(pulldown_cmark::Options::ENABLE_TABLES)
        }
        if self.enable_footnotes {
            opts.toggle(pulldown_cmark::Options::ENABLE_FOOTNOTES)
        }
        if self.enable_strikethrough {
            opts.toggle(pulldown_cmark::Options::ENABLE_STRIKETHROUGH)
        }
        if self.enable_tasklists {
            opts.toggle(pulldown_cmark::Options::ENABLE_TASKLISTS)
        }
        if self.enable_smart_punctuation {
            opts.toggle(pulldown_cmark::Options::ENABLE_SMART_PUNCTUATION)
        }
        if self.enable_heading_attributes {
            opts.toggle(pulldown_cmark::Options::ENABLE_HEADING_ATTRIBUTES)
        }
        opts
    }
}