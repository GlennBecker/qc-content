use std::{
    sync::{
        mpsc::{Receiver, Sender},
        Arc, Mutex,
    },
    thread::JoinHandle,
    time::Instant,
};

use crate::{
    config::Config, jobs, stats::Stats, store::ContentStore, type_definitions::TypeGenerator,
};

pub struct ThreadPool {
    start: Instant,
    stats: Arc<Mutex<Stats>>,
    sender: Sender<Job>,
    workers: Vec<Worker>,
}

impl ThreadPool {
    pub fn new(stats: Arc<Mutex<Stats>>) -> Self {
        let threads = std::thread::available_parallelism()
            .map(|i| i.into())
            .unwrap_or(4);
        let mut workers = Vec::with_capacity(threads);
        let (sender, receiver) = std::sync::mpsc::channel::<Job>();
        let receiver = Arc::new(Mutex::new(receiver));
        for id in 0..threads {
            workers.push(Worker::new(id, Arc::clone(&receiver)))
        }
        Self {
            start: Instant::now(),
            sender,
            stats,
            workers,
        }
    }
    pub fn execute(&self, job: Job) {
        self.sender.send(job).unwrap()
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        for _ in &mut self.workers {
            self.sender.send(Job::Terminate).unwrap();
        }
        for worker in &mut self.workers {
            // println!("Shutting down worker {}", worker.id);
            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
        println!("{}", self.stats.lock().unwrap());
        println!(
            "Finished in {:.3}ms",
            self.start.elapsed().as_nanos() as f32 / 1000000.0
        )
    }
}

#[derive(Debug)]
struct Worker {
    #[allow(dead_code)]
    id: usize,
    thread: Option<JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<Receiver<Job>>>) -> Self {
        let thread = std::thread::spawn(move || loop {
            let job = receiver.lock().unwrap().recv().unwrap();
            // println!("Worker {} got a job; executing.", id);
            match job {
                Job::ProcessCollections(store, config, stats, type_gen) => {
                    jobs::collections::generate(store, config, stats, type_gen)
                }
                Job::ProcessTaxonomies(store, config, stats, type_gen) => {
                    jobs::taxonomies::generate(store, config, stats, type_gen)
                }
                Job::ProcessContent(store, config, stats) => {
                    jobs::content::generate(store, config, stats)
                }
                Job::Terminate => {
                    break;
                }
            }
        });
        Self {
            id,
            thread: Some(thread),
        }
    }
}

pub enum Job {
    ProcessCollections(
        Arc<ContentStore>,
        Arc<Config>,
        Arc<Mutex<Stats>>,
        Arc<Mutex<TypeGenerator>>,
    ),
    ProcessTaxonomies(
        Arc<ContentStore>,
        Arc<Config>,
        Arc<Mutex<Stats>>,
        Arc<Mutex<TypeGenerator>>,
    ),
    ProcessContent(Arc<ContentStore>, Arc<Config>, Arc<Mutex<Stats>>),
    Terminate,
}
