use crate::err;
use std::{path::{Path, PathBuf}, collections::BTreeMap};

/*
  A Store that contains all the content data in a single string,
  and a Vec of Pages, which are indexes into the data string
*/

#[derive(Debug, Default)]
pub struct ContentStore {
    pub store: String,
    pub config: BTreeMap<PathBuf, serde_yaml::Mapping>,
    pub paths: Vec<(usize, usize)>,
    pub frontmatter: Vec<(usize, usize)>,
    pub imports: Vec<(usize, usize)>,
    pub bodies: Vec<(usize, usize)>,
}

impl ContentStore {
    pub fn len(&self) -> usize {
        self.paths.len()
    }
    pub fn is_empty(&self) -> bool {
        self.store.is_empty()
    }
    pub fn push_config<P: AsRef<Path>>(&mut self, path: P, src: &str) -> Result<(), err::Error> {
        let value = serde_json::from_str(src)?;
        self.config.insert(path.as_ref().to_path_buf(), value);
        Ok(())
    }
    pub fn push_path<P: AsRef<Path>>(&mut self, path: P) {
        let start = self.store.len();
        let path_str = path.as_ref().to_string_lossy();
        self.store.push_str(&path_str);
        let end = self.store.len();
        self.paths.push((start, end));
        println!("{:?}", self);
    }
    pub fn push_md(&mut self, src: &str) {
        let start = self.store.len();
        let frontmatter_offset = get_frontmatter_range(src) as usize + start;
        self.frontmatter.push((start, frontmatter_offset));
        self.imports.push((0, 0));
        
        let len: usize = src.chars().map(|c| c.len_utf8()).sum();
        self.bodies.push((frontmatter_offset, start + len));
        self.store.push_str(src)
    }
    pub fn push_mdx(&mut self, src: &str) {
        let start = self.store.len();
        let frontmatter_offset = get_frontmatter_range(src) as usize;
        let import_offset =
            get_imports_range(&src[frontmatter_offset..]) as usize + frontmatter_offset + start;

        self.frontmatter.push((start, frontmatter_offset + start));
        self.imports
            .push((frontmatter_offset + start, import_offset));
        
        let body_len: usize = src.chars().map(|s| s.len_utf8()).sum();
        println!("{} {}", body_len, src.len());
        self.bodies.push((import_offset, body_len + start));
        self.store.push_str(src)
    }
    pub fn push_file<P: AsRef<Path>>(&mut self, path: P, src: &str) -> Result<(), err::Error> {
        match path.as_ref().extension().and_then(|ext| ext.to_str()) {
            Some("md") => {
                println!("md");
                self.push_path(path);
                self.push_md(src);
                Ok(())
            }
            Some("mdx") => {
                println!("mdx");
                self.push_path(path);
                self.push_mdx(src);
                Ok(())
            }
            None => {
                println!("directory");
                return Err(err::Error::CouldNotReadExtension(
                    path.as_ref().to_path_buf(),
                ))
            }
            _ => {
                println!("invalid");
                return Err(err::Error::UnsupportedExtension(
                    path.as_ref().to_path_buf(),
                ))
            }
        }
    }
    pub fn read(&self, range: (usize, usize)) -> &str {
        &self.store[range.0..range.1]
    }
    pub fn paths(&self) -> std::slice::Iter<(usize, usize)> {
        self.paths.iter()
    }
    pub fn frontmatter(&self) -> std::slice::Iter<(usize, usize)> {
        self.frontmatter.iter()
    }
    pub fn imports(&self) -> std::slice::Iter<(usize, usize)> {
        self.imports.iter()
    }
    pub fn bodies(&self) -> std::slice::Iter<(usize, usize)> {
        self.bodies.iter()
    }
}

/*
Gets the offset from the start of file "---"" to the closing "---" if.
Does not validate if yaml if well formed.
*/
fn get_frontmatter_range(src: &str) -> usize {
    if !src.starts_with("---") {
        return 0;
    }
    let mut offset = 3;
    let additional = src[offset..].chars().take_while(|c| c == &'-').count();
    offset += additional;
    for slice in src[offset..].as_bytes().windows(3) {
        offset += 1;
        if slice == b"---" {
            offset += 2;
            break;
        }
    }
    let additional = src[offset..].chars().take_while(|c| c == &'-').count();
    offset + additional
}

/*
  Gets the offset of imports at the top of the document.
  Does not validate that the imports are well formed.
*/
fn get_imports_range(src: &str) -> u16 {
    let mut lines = 0;
    let mut line_lens = 0;
    for line in src.lines() {
        if line.is_empty() || line.starts_with("import") {
            let len_u16: u16 = line.len().try_into().unwrap();
            lines += 1;
            line_lens += len_u16;
            continue;
        }
        break;
    }
    line_lens + lines
}

#[cfg(test)]
mod test {
    use super::{get_frontmatter_range, get_imports_range, ContentStore};
    use proptest::prelude::*;

    #[test]
    fn gets_imports() {
        let src = vec![
            "import * as A from \"./whatever\"",
            "import { B, C } from 'other-module'",
            "import Default from '@stuff'",
            "",
        ]
        .join("\n");
        let offset = get_imports_range(&src);
        assert_eq!(&src[0..offset as usize], &src)
    }
    #[test]
    fn can_recover_mdx() {
        let path = "src/content/blog/first-post.mdx";
        let frontmatter = vec![
            "---",
            "title: First Post",
            "draft: false",
            "tags: [blog, qwik, qwik-city]",
            "---",
        ]
        .join("\n");
        let imports = vec![
            "import * as A from \"./whatever\"",
            "import { B, C } from 'other-module'",
            "import Default from '@stuff'",
        ]
        .join("\n");
        let body = "This is the first post".to_owned();
        let src: String = format!("{}\n\n{}\n\n{}", &frontmatter, &imports, &body);
        let mut store = ContentStore::default();
        assert!(store.push_file(path, &src).is_ok());
        for (((path_r, frontmatter_r), imports_r), body_r) in store
            .paths()
            .zip(store.frontmatter())
            .zip(store.imports())
            .zip(store.bodies())
        {
            assert_eq!(&path, &store.read(*path_r));
            assert_eq!(&frontmatter, &store.read(*frontmatter_r));
            assert_eq!(&imports, &store.read(*imports_r).trim());
            assert_eq!(&body, store.read(*body_r).trim());
        }
    }
    proptest! {
        #[test]
        fn can_recover_any_md(path in ".*", frontmatter in ".*", body in ".*") {
            let mut store = ContentStore::default();
            let md = format!("---\n{}\n---\n{}", frontmatter, body);
            store.push_path(&path);
            store.push_md(&md);
            let path_r = store.paths().next().unwrap();
            let frontmatter_r = store.frontmatter().next().unwrap();
            let body_r = store.bodies().next().unwrap();
            prop_assert!(store.read(*path_r) == path);
            prop_assert!(store.read(*frontmatter_r).trim_matches(|c| c == '-').trim() == frontmatter.trim());
            prop_assert!(store.read(*body_r).trim() == body.trim());

        }
        #[test]
        fn get_all_frontmatter_fences(start_num: u8, inner in ".*", end_num: u8) {
            let start_fence: String = (0..start_num).map(|_| "-").collect();
            let end_fence: String = (0..end_num).map(|_| "-").collect();
            let src = &[&format!("---{}",&start_fence), &inner, &format!("---{}",&end_fence), ""].join("\n");
            let offset = get_frontmatter_range(src);
            assert_eq!(&src[0..offset as usize], src.trim())
        }
    }
}