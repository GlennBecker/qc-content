use std::{
    collections::{HashMap},
    fs::File,
    io::{BufWriter, Write},
    sync::{
        mpsc::{Receiver, Sender},
        Arc, Mutex,
    },
    thread::JoinHandle,
};

use crate::{
    fmt::{write_pascal_case},
    stats::Stats,
    store::ContentStore,
    type_inference::{combine, TSTypeDef},
};

pub enum Job {
    WritePrelude(Arc<Mutex<BufWriter<File>>>),
    GenerateType(
        Arc<ContentStore>,
        Arc<Mutex<BufWriter<File>>>,
        String,
        Vec<usize>,
    ),
    Terminate,
}

/*
Type Generator that merges frontmatter for content arrays and outputs types.
*/

#[derive(Debug)]
struct Worker {
    #[allow(dead_code)]
    id: usize,
    thread: Option<JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<Receiver<Job>>>) -> Self {
        let thread = std::thread::spawn(move || loop {
            let job = receiver.lock().unwrap().recv().unwrap();
            match job {
                Job::WritePrelude(writer) => match std::fs::read("src/types_prelude.ts") {
                    Ok(prelude) => {
                        let mut w = writer.lock().unwrap();
                        let w = w.get_mut();
                        w.write_all(&prelude);
                    }
                    Err(e) => {
                        println!("{}", e)
                    }
                },
                Job::GenerateType(store, writer, name, ids) => {
                    let mut merged = TSTypeDef::Unknown;
                    for id in ids.iter() {
                        if let Some(frontmatter) = store.frontmatter().nth(*id) {
                            let trimmed = store.read(*frontmatter).trim_matches(|c| c == '-');
                            if let Ok(mapping) = serde_yaml::from_str::<serde_yaml::Value>(trimmed)
                            {
                                let type_def = TSTypeDef::from_serde_yaml(&mapping);
                                merged = combine(merged, type_def.unwrap());
                            } else {
                                println!("Bad Yaml:\n\n{}", store.read(*frontmatter))
                            }
                        } else {
                            println!("No frontmatter found for {}", id)
                        }
                    }
                    if !matches!(merged, TSTypeDef::Unknown) {
                        let mut file = writer.lock().unwrap();
                        let mut file = file.get_mut();
                        let _ = file.write_all(b"export interface ");
                        let _ = write_pascal_case(&mut file, &mut name.chars());
                        let _ = file.write_fmt(format_args!(" extends Page {}\n\n", merged));
                        let _ = file.flush();
                    } else {
                        println!("Could not determine type for {}", name)
                    }
                }
                Job::Terminate => {
                    break;
                }
            }
        });
        Self {
            id,
            thread: Some(thread),
        }
    }
}

pub struct TypeGenerator {
    content: Arc<ContentStore>,
    cache: Arc<Mutex<HashMap<usize, TSTypeDef>>>,
    file: Arc<Mutex<BufWriter<File>>>,
    stats: Arc<Mutex<Stats>>,
    worker: Worker,
    sender: Sender<Job>,
}

impl TypeGenerator {
    pub fn new(content: Arc<ContentStore>, stats: Arc<Mutex<Stats>>, file: File) -> Self {
        let (sender, receiver) = std::sync::mpsc::channel::<Job>();
        let receiver = Arc::new(Mutex::new(receiver));
        let worker = Worker::new(0, Arc::clone(&receiver));
        let writer = Arc::new(Mutex::new(BufWriter::new(file)));
        if let Err(e) = sender.send(Job::WritePrelude(writer.clone())) {
            println!("{}", e);
        }
        Self {
            content,
            worker,
            sender,
            file: writer,
            stats,
            cache: Arc::new(Mutex::new(HashMap::default())),
        }
    }
    pub fn generate(&self, typename: String, files: Vec<usize>) {
        if let Err(e) =self.sender.send(Job::GenerateType(
            self.content.clone(),
            self.file.clone(),
            typename,
            files,
        )) {
            println!("{}", e);
        };
    }
}

impl Drop for TypeGenerator {
    fn drop(&mut self) {
        self.sender.send(Job::Terminate).unwrap();
        if let Some(thread) = self.worker.thread.take() {
            thread.join().unwrap();
        }
    }
}
