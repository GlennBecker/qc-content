#![allow(dead_code)]
#![allow(unused_imports, unused_must_use, unused_variables)]

mod config;
mod err;
mod traits;
mod fmt;
mod jobs;
mod mdx;
mod routeparams;
mod stats;
mod store;
mod threadpool;
mod type_definitions;
mod type_inference;
mod codegen;

use config::Config;
use routeparams::RouteParams;
use stats::Stats;
use std::{
    fs::File,
    io::{BufWriter, Write},
    path::Path,
    sync::{Arc, Mutex},
};
use type_definitions::TypeGenerator;

use crate::{
    store::ContentStore,
    threadpool::{Job, ThreadPool},
};

pub fn generate_route_types<P: AsRef<Path>>(dir: P) {
    match std::fs::read_dir(dir.as_ref()) {
        Ok(directory) => {
            for item in directory.filter_map(|i| i.ok()) {
                let path = item.path();
                if path.is_file() && path.file_stem().and_then(|n| n.to_str()) == Some("index") {
                    if let Some(parent) = path.parent().map(|p| p.join("gen-types.ts")) {
                        let mut params = RouteParams::new(&parent);
                        if let Some(next) = params.next() {
                            match std::fs::File::create(&parent) {
                                Ok(file) => {
                                    let mut writer = BufWriter::new(file);
                                    let _ = writer.write_all(b"export interface RouteParams extends Record<string, string | undefined> {\n");
                                    let _ = writer.write_fmt(format_args!("{}", next));
                                    for param in params {
                                        let _ = writer.write_fmt(format_args!(",\n{}", param));
                                    }
                                    let _ = writer.write_all(b"\n}");
                                    let _ = writer.flush();
                                }
                                Err(e) => {
                                    println!("{}", e)
                                }
                            }
                        }
                    }
                }
                if path.is_dir() {
                    generate_route_types(path);
                }
            }
        }
        Err(e) => {
            println!("{}", e);
        }
    }
}

pub fn process_content(config: Config) {
    let mut store = ContentStore::default();
    let mut stats = Stats::new();

    let _ = std::fs::remove_dir_all(&config.output_folder);

    if let Err(e) = std::fs::create_dir_all(&config.output_folder) {
        stats.errors.push(e.into());
    }

    gather_content_rec(&config.content_folder, &mut store, &mut stats);

    let config = Arc::new(config);
    let store = Arc::new(store);
    let stats = Arc::new(Mutex::new(stats));
    let threadpool = ThreadPool::new(stats.clone());
    let outpath: &Path = config.output_folder.as_ref();
    let file = File::create(outpath.join("types.ts")).unwrap();
    let type_generator = Arc::new(Mutex::new(TypeGenerator::new(
        store.clone(),
        stats.clone(),
        file,
    )));
    threadpool.execute(Job::ProcessContent(
        store.clone(),
        config.clone(),
        stats.clone(),
    ));
    threadpool.execute(Job::ProcessCollections(
        store.clone(),
        config.clone(),
        stats.clone(),
        type_generator.clone(),
    ));
    threadpool.execute(Job::ProcessTaxonomies(store, config, stats, type_generator));
}

// Walks the input directory and adds all content files to the store.
pub fn gather_content_rec<P: AsRef<Path>>(
    directory: P,
    store: &mut ContentStore,
    stats: &mut Stats,
) {
    match std::fs::read_dir(&directory) {
        Ok(dir) => {
            for entry in dir.filter_map(|e| e.ok()) {
                let path = entry.path();
                if path.is_dir() {
                    gather_content_rec(&path, store, stats);
                }
                if path.is_file() {
                    match std::fs::read_to_string(&path) {
                        Ok(file) => {
                            if path.file_name().and_then(|p| p.to_str()) == Some("frontmatter.json") {
                                println!("Config: {}", path.display());
                                if let Err(e) = store.push_config(path.parent().unwrap(), &file) {
                                    stats.errors.push(e)
                                }
                            } else if matches!(path.extension().and_then(|e| e.to_str()), Some("md") | Some("mdx")) {                                
                                if let Err(e) = store.push_file(&path, &file) {
                                stats.errors.push(e);
                            }
                        }
                    },
                        Err(e) => stats.errors.push(e.into()),
                    }
                }
            }
        }
        Err(e) => stats.errors.push(e.into()),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn docs_example() {
        let config = Config {
            content_folder: "examples/docs/src/content".to_owned(),
            output_folder: "examples/docs/src/gen-content".to_owned(),
            ..Default::default()
        };

        process_content(config);
    }

    #[test]
    fn docs_params_example() {
        generate_route_types("examples/docs/src/routes");
    }
}
