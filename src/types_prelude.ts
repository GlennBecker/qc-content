import { QwikJSX } from "@builder.io/qwik/";

export type ContentArray = (QwikJSX.Element | string)[];

export interface Page {
  _id: string,
  _path: string,
  _slug: string,
  _extension: string,
  _raw: string,
  _content: ContentArray,
  _headings: {id: string, text: string}[]
}

