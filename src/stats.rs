use std::fmt::Display;

use crate::err::Error;

#[derive(Debug)]
pub struct Stats {
    pub init: std::time::Instant,
    pub content_elapsed: u128,
    pub collections_elapsed: u128,
    pub taxonomies_elapsed: u128,
    pub total_collections: usize,
    pub total_taxonomies: usize,
    pub total_files: usize,
    pub errors: Vec<Error>,
}

impl Stats {
    pub fn new() -> Self {
        Self {
            init: std::time::Instant::now(),
            content_elapsed: 0,
            collections_elapsed: 0,
            taxonomies_elapsed: 0,
            total_collections: 0,
            total_taxonomies: 0,
            total_files: 0,
            errors: Vec::default(),
        }
    }
}

impl Default for Stats {
    fn default() -> Self {
        Self::new()
    }
}

impl Display for Stats {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Qwik City Content\n")?;
        f.write_fmt(format_args!(
            "  File processed: {} in {:.1}ms\n",
            self.total_files,
            self.content_elapsed as f32 / 1000.0
        ))?;
        f.write_fmt(format_args!(
            "  Taxonomies Generated: {} in {:.1}ms\n",
            self.total_taxonomies,
            self.taxonomies_elapsed as f32 / 1000.0
        ))?;
        f.write_fmt(format_args!(
            "  Collections Generated: {} in {:.1}ms\n",
            self.total_collections,
            self.collections_elapsed as f32 / 1000.0
        ))?;
        f.write_fmt(format_args!("  Total errors: {}\n", self.errors.len()))?;
        f.write_fmt(format_args!(
            "  Total time: {:.1}ms\n",
            self.init.elapsed().as_millis() as f32 / 1000.0
        ))?;
        Ok(())
    }
}
