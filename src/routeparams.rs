use std::{fmt::Display, path::Path};

#[derive(Debug, PartialEq, Eq)]
pub enum Param<'a> {
    Param(&'a str),
    Wildcard(&'a str),
}

impl<'a> Display for Param<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Param::Param(param) => f.write_fmt(format_args!("  \"{}\": string", param)),
            Param::Wildcard(param) => f.write_fmt(format_args!("  \"{}\"?: string", param)),
        }
    }
}

pub struct RouteParams<'a> {
    inner: std::path::Iter<'a>,
}

impl<'a> RouteParams<'a> {
    pub fn new<P: AsRef<Path>>(path: &'a P) -> Self {
        let p = if path.as_ref().is_file() {
            path.as_ref().parent().unwrap_or_else(|| Path::new("/"))
        } else {
            path.as_ref()
        };
        Self { inner: p.iter() }
    }
}

impl<'a> Iterator for RouteParams<'a> {
    type Item = Param<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        for segment in self.inner.by_ref() {
            if let Some(param) = segment
                .to_str()
                .and_then(|s| s.strip_prefix('['))
                .and_then(|s| s.strip_suffix(']'))
            {
                if let Some(param) = param.strip_prefix("...") {
                    return Some(Param::Wildcard(param));
                }
                return Some(Param::Param(param));
            }
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use super::RouteParams;

    #[test]
    fn it_works_with_dir() {
        let path = "/[my]/[param]/[and]/[...wildcard]";
        for param in RouteParams::new(&path) {
            println!("{:?}", param)
        }
    }
    #[test]
    fn it_works_with_index() {
        let path = "/[my]/[param]/[and]/[...wildcard]/index.tsx";
        for param in RouteParams::new(&path) {
            println!("{:?}", param)
        }
    }
}
