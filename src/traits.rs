use crate::err;

pub trait WriteJSON {
    fn write_js<W: std::io::Write>(&self, w: &mut W) -> Result<(), err::Error>;
}