use std::{iter::Peekable, borrow::Borrow};

use pulldown_cmark::{LinkType, CowStr};
use std::collections::HashMap;

use crate::{err, fmt::{to_kebab_case}};

#[derive(Debug, PartialEq, Eq)]
pub enum Content {
    Markdown(String),
    Jsx(Vec<String>),
}

pub enum TableState {
    Head,
    Body,
}

pub struct MDXParser<'a, I: Iterator> {
    iter: Peekable<I>,
    buffer: String,
    content: Vec<Content>,
    alignments: Vec<pulldown_cmark::Alignment>,
    table_state: TableState,
    table_cell_index: usize,
    numbers: HashMap<CowStr<'a>, usize>,
    headings: Vec<(String, String)>
}

impl<'a, I> MDXParser<'a, I> where I: Iterator<Item = pulldown_cmark::Event<'a>> {
    pub fn new(iter: I) -> Self {
        Self {
            iter: iter.peekable(),
            buffer: String::new(),
            content: Vec::default(),
            alignments: Vec::default(),
            table_state: TableState::Head,
            table_cell_index: 0,
            numbers: HashMap::default(),
            headings: Vec::default()
        }
    }
    pub fn write(&mut self, str: &str) -> Result<(), err::Error> {
        self.buffer.push_str(str);
        Ok(())
    }
    pub fn get_content(mut self) -> Result<(Vec<Content>, Vec<(String, String)>), err::Error> {
        while let Some(event) = self.iter.next() {
            match event {
                pulldown_cmark::Event::Start(tag) => {self.start_tag(tag)?;},
                pulldown_cmark::Event::End(tag) => {self.end_tag(tag)?;},
                pulldown_cmark::Event::Text(text) => {
                    self.buffer.push_str(&text)
                },
                pulldown_cmark::Event::Code(code) => {
                    self.buffer.push_str(&format!("<code>{}</code>", code));
                },
                pulldown_cmark::Event::Html(html) => {
                    if !self.buffer.is_empty() {
                        let buff = std::mem::take(&mut self.buffer);
                        self.content.push(Content::Markdown(buff));
                    }
                    let mut jsx = vec![html.to_string()];
                    while let Some(pulldown_cmark::Event::Html(html)) = self.iter.peek() {
                        jsx.push(html.to_string());
                        self.iter.next();
                    }
                    self.content.push(Content::Jsx(jsx));
                },
                pulldown_cmark::Event::FootnoteReference(name) => {
                    let len = self.numbers.len() + 1;
                    self.write("<sup class=\"footnote-reference\"><a href=\"#")?;
                    self.write(&name)?;
                    // escape_html(&mut self.writer, &name)?;
                    self.write("\">")?;
                    let number = *self.numbers.entry(name).or_insert(len);
                    self.write(&format!("{}", number))?;
                    // write!(&mut self.writer, "{}", number)?;
                    self.write("</a></sup>")?;
                },
                pulldown_cmark::Event::SoftBreak => {},
                pulldown_cmark::Event::HardBreak => {self.write("</br>")?;},
                pulldown_cmark::Event::Rule => {self.write("<hr/>")?},
                pulldown_cmark::Event::TaskListMarker(true) => {
                    self.write("<input type=\"checkbox\" disabled=\"\" checked=\"\"/>")?;
                },
                pulldown_cmark::Event::TaskListMarker(false) => {
                    self.write("<input type=\"checkbox\" disabled=\"\"/>")?;
                },
            }
        }
        if !self.buffer.is_empty() {
            self.content.push(Content::Markdown(self.buffer));
        }
        Ok((self.content, self.headings))
    }
    pub fn start_tag(&mut self, tag: pulldown_cmark::Tag<'a>) -> Result<(), err::Error> {
        match tag {
            pulldown_cmark::Tag::Paragraph => self.buffer.push_str("<p>"),
            pulldown_cmark::Tag::Heading(level, id, classes) => {
                self.write(&format!("<{}", level))?;
                let mut text = String::new();
                while let Some(pulldown_cmark::Event::Text(value)) = self.iter.peek() {
                    text.push_str(value.borrow());
                    self.iter.next();
                }
                let id = id.map(|s| s.to_owned()).unwrap_or_else(||to_kebab_case(&text));
                self.write(&format!(" id=\"{}\"", id))?;
                let mut class_iter = classes.into_iter();
                if let Some(first) = class_iter.next() {
                    self.write("class=\"")?;
                    self.write(first)?;
                    for class in class_iter {
                        self.write(" ")?;
                        self.write(class)?;
                    }
                    self.write("\"")?;
                }
                self.write(">")?;
                self.write(&text)?;
                self.headings.push((id, text))
            },
            pulldown_cmark::Tag::BlockQuote => {
                self.write("<blockquote>")?;
            },
            pulldown_cmark::Tag::CodeBlock(kind) => {
                match kind {
                    pulldown_cmark::CodeBlockKind::Indented => {
                        self.buffer.push_str("<pre><code>")
                    },
                    pulldown_cmark::CodeBlockKind::Fenced(info) => {
                        let lang = info.split(' ').next().unwrap();
                        if lang.is_empty() {
                            self.write("<pre><code>")?;
                        } else {
                            self.write("<pre><code class=\"language-")?;
                            self.write(lang)?;
                            // escape_html(&mut writer, lang)?;
                            self.write("\">")?;
                        }
                    },
                }
            },
            pulldown_cmark::Tag::List(Some(1)) => {
                self.write("<ol>")?;
            }
            pulldown_cmark::Tag::List(Some(start)) => {
                self.write(&format!("<ol start=\"{}\"", start))?;
            }
            pulldown_cmark::Tag::List(None) => {
                self.write("<ul>")?;
            },
            pulldown_cmark::Tag::Item => {self.write("<li>")?;},
            pulldown_cmark::Tag::FootnoteDefinition(name) => {
                self.write("<div class=\"footnote-definition\" id=\"")?;
                // escape_html(&mut self.writer, &*name)?;
                self.write(&name)?;
                self.write("\"><sup class=\"footnote-definition-label\">")?;
                let len = self.numbers.len() + 1;
                let number = *self.numbers.entry(name).or_insert(len);
                self.write(&format!("{}", number))?;
                self.write("</sup>")?;                
            },
            pulldown_cmark::Tag::Table(alignments) => {
                self.alignments = alignments;
                self.write("<table>")?;
            },
            pulldown_cmark::Tag::TableHead => {
                self.table_state = TableState::Head;
                self.table_cell_index = 0;
                self.write("<thead><tr>")?;
            },
            pulldown_cmark::Tag::TableRow => {
                self.table_cell_index = 0;
                self.write("<tr>")?;
            },
            pulldown_cmark::Tag::TableCell => {
                match self.table_state {
                    TableState::Head => {
                        self.write("<th")?;
                    }
                    TableState::Body => {
                        self.write("<td")?;
                    }
                }
                match self.alignments.get(self.table_cell_index) {
                    Some(&pulldown_cmark::Alignment::Left) => {self.write(" style=\"text-align: left\">")?;},
                    Some(&pulldown_cmark::Alignment::Center) => {self.write(" style=\"text-align: center\">")?;},
                    Some(&pulldown_cmark::Alignment::Right) => {self.write(" style=\"text-align: right\">")?;},
                    _ => {self.write(">")?;},
                }
            },
            pulldown_cmark::Tag::Emphasis => {self.write("<em>")?;},
            pulldown_cmark::Tag::Strong => {self.write("<strong>")?;},
            pulldown_cmark::Tag::Strikethrough => {self.write("<del>")?;},
            pulldown_cmark::Tag::Link(LinkType::Email, email, title) => {
                self.write("<a href=\"mailto:")?;
                self.write(&email)?;
                self.write("\" title=\"")?;
                self.write(&title)?;
                self.write("\">")?;
            }
            pulldown_cmark::Tag::Link(_, href, title) => {
                self.write("<a href=\"")?;
                self.write(&href)?;
                self.write("\" title=\"")?;
                self.write(&title)?;
                self.write("\">")?;
            },
            pulldown_cmark::Tag::Image(_, src, _) => {
                self.write("<img src=\"")?;
                self.write(&src)?;
                self.write("\" alt=\"")?;
                let mut alt = String::new();
                while let Some(pulldown_cmark::Event::Text(text)) = self.iter.next() {
                    alt.push_str(&text);
                }
                self.write(&alt)?;
                self.write("\"/>")?;
            },
        }
        Ok(())
    }
    pub fn end_tag(&mut self, tag: pulldown_cmark::Tag<'a>) -> Result<(), err::Error> {
        match tag {
            pulldown_cmark::Tag::Paragraph => {self.write("</p>")?;},
            pulldown_cmark::Tag::Heading(level, _, _) => {self.write(&format!("</{}>", level))?;},
            pulldown_cmark::Tag::BlockQuote => {self.write("</blockquote>")?;},
            pulldown_cmark::Tag::CodeBlock(_) => {self.write("</code></pre>")?;},
            pulldown_cmark::Tag::List(start) => {
                match start {
                    Some(_) => {self.write("</ol>")?;}
                    None => {self.write("</ul>")?;}
                }
            },
            pulldown_cmark::Tag::Item => {self.write("</li>")?;},
            pulldown_cmark::Tag::FootnoteDefinition(_) => {
                self.write("</div>")?;
            },
            pulldown_cmark::Tag::Table(_) => {
                self.write("</tbody></table>")?;
            },
            pulldown_cmark::Tag::TableHead => {
                self.write("</tr></thead><tbody>")?;
                self.table_state = TableState::Body;
            },
            pulldown_cmark::Tag::TableRow => {
                self.write("</tr>")?;
            },
            pulldown_cmark::Tag::TableCell => {
                match self.table_state {
                    TableState::Head => {
                        self.write("</th>")?;
                    }
                    TableState::Body => {
                        self.write("</td>")?;
                    }
                }
                self.table_cell_index += 1;
            },
            pulldown_cmark::Tag::Emphasis => {self.write("</em>")?;},
            pulldown_cmark::Tag::Strong => {self.write("</strong>")?;},
            pulldown_cmark::Tag::Strikethrough => {self.write("</del>")?;},
            pulldown_cmark::Tag::Link(_, _, _) => {
                self.write("</a>")?;
            },
            pulldown_cmark::Tag::Image(_, _, _) => {},
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use proptest::{proptest, prop_assert};

    use super::*;
    #[test]
    fn mdx() {
        let src = "<article>\n  <section>\n    <div>\n      <p>Mdx Stuff</p>\n    </div>\n  </section>\n</article>";
        let parser = pulldown_cmark::Parser::new(src);
        let mdx_parser = MDXParser::new(parser);
        let content = mdx_parser.get_content();
        println!("{:?}", content);
    }

    #[test]
    fn heading() {
        let src = "# A heading";
        let parser = pulldown_cmark::Parser::new(src);
        let mdx_parser = MDXParser::new(parser);
        let content = mdx_parser.get_content();
        println!("{:?}", content);
    }

    #[test]
    fn ol_starting_from_one() {
        let src = vec![
            "1. Some thing",
            "2. Another thing",
            "3. Third times a charm"
        ]
        .join("\n");
        let parser = pulldown_cmark::Parser::new(&src);
        let mdx_parser = MDXParser::new(parser);
        let content = mdx_parser.get_content();
        println!("{:?}", content);
    }
    #[test]
    fn image() {
        let src = "![Tux, the Linux mascot](/assets/images/tux.png)";
        let parser = pulldown_cmark::Parser::new(src);
        let mdx_parser = MDXParser::new(parser);
        let content = mdx_parser.get_content();
        println!("{:?}", content);
    }
    #[test]
    fn link() {
        let src = "[Tux, the Linux mascot](/assets/images/tux.png)";
        let parser = pulldown_cmark::Parser::new(src);
        let mdx_parser = MDXParser::new(parser);
        let content = mdx_parser.get_content();
        println!("{:?}", content);
    }
    #[test]
    fn fenced_code() {
        let src = vec!["```typescript", 
        "  console.log(\"Hello world\");",
        "```"].join("\n");
        let parser = pulldown_cmark::Parser::new(&src);
        let mdx_parser = MDXParser::new(parser);
        let content = mdx_parser.get_content();
        println!("{:?}", content)
    }
    #[test]
    fn headings() {
        let src = "# Some **heading with [a link](http://website.com)**";
        for item in pulldown_cmark::Parser::new(src) {
            println!("{:?}", item);
        }
    }
    proptest! {
        #[test]
        fn handles_any_string(src in ".*") {
            let parser = pulldown_cmark::Parser::new(&src);
            let mdx_parser = MDXParser::new(parser);
            let content = mdx_parser.get_content();
            prop_assert!(content.is_ok())
        }
    }
}