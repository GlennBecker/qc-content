use serde::de::{self, Visitor};
use serde::Deserialize;

use crate::codegen::{WriteTypescript, CodeWriter};
use std::{
    collections::{HashMap, HashSet},
    fmt::{Display, Write},
    hash::Hash,
};

#[derive(Debug, Eq, PartialEq, Clone, Copy, Hash)]
pub enum Primitive {
    Null,
    Boolean,
    Number,
    String,
}

impl Display for Primitive {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Null => f.write_str("null"),
            Self::Boolean => f.write_str("boolean"),
            Self::Number => f.write_str("number"),
            Self::String => f.write_str("string"),
        }
    }
}

impl WriteTypescript for Primitive {
    fn write_ts<W: std::io::Write>(&self, w: &mut crate::codegen::CodeWriter<W>) -> std::io::Result<()> {
        match self {
            Self::Null => w.write_code("null"),
            Self::Boolean => w.write_code("boolean"),
            Self::Number => w.write_code("number"),
            Self::String => w.write_code("string"),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Choice {
    array: Option<Box<TSTypeDef>>,
    object: HashMap<TSField, TSTypeDef>,
    primitives: HashSet<Primitive>,
}

impl PartialEq for Choice {
    fn eq(&self, other: &Self) -> bool {
        for item in &other.primitives {
            if !self.primitives.contains(item) {
                return false;
            }
        }
        self.array == other.array
            && self.object == other.object
            && self.primitives.len() == other.primitives.len()
    }
}

/*
TSTypeDef is a monoid under the combine function.
    An empty object is the zero element such that:
        combine(TSTypeDef, Zero) === combine(Zero, TSTypeDef) === TSTypeDef
    The combine function is associative
*/
#[derive(Clone, Debug)]
pub enum TSTypeDef {
    Primitive(Primitive),
    Object(HashMap<TSField, TSTypeDef>),
    Array(Box<TSTypeDef>),
    Choice(Choice),
    Unknown,
}

impl PartialEq for TSTypeDef {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Object(l0), Self::Object(r0)) => l0 == r0,
            (Self::Array(l0), Self::Array(r0)) => l0 == r0,
            (Self::Choice(l0), Self::Choice(r0)) => l0 == r0,
            (Self::Primitive(l0), Self::Primitive(r0)) => l0 == r0,
            _ => core::mem::discriminant(self) == core::mem::discriminant(other),
        }
    }
}

impl WriteTypescript for TSTypeDef {
    fn write_ts<W: std::io::Write>(&self, w: &mut CodeWriter<W>) -> std::io::Result<()> {
        fn write_ts_rec<W: std::io::Write>(type_def: &TSTypeDef, w: &mut CodeWriter<W>) -> std::io::Result<()> {
            match type_def {
                TSTypeDef::Primitive(p) => {
                    p.write_ts(w)?;
                },
                TSTypeDef::Object(hash_map) => {
                    w.start_obj()?;
                    for (key, value) in hash_map.iter() {
                        key.write_ts(w)?;
                        write_ts_rec(value, w)?;
                        w.end_key_value()?;
                    }
                    w.end_obj()?;
                },
                TSTypeDef::Array(ts) => {
                    write_ts_rec(&ts, w)?;
                    w.write_code("[]")?;
                },
                TSTypeDef::Choice(choice) => {
                    let mut primitives = choice.primitives.iter();
                    if let Some(first) = primitives.next() {
                        first.write_ts(w)?;
                        for choice in primitives {
                            w.write_code(" | ")?;
                            choice.write_ts(w)?;
                        }
                    }
                    if let Some(array) = &choice.array {
                        if !choice.primitives.is_empty() {
                            w.write_code(" | ")?;
                        }
                        array.write_ts(w);
                        w.write_code("[]")?;
                    }
                    if !choice.object.is_empty() {
                        if choice.array.is_some() || !choice.primitives.is_empty() {
                            w.write_code(" | ")?;
                        }
                        write_ts_rec(&TSTypeDef::Object(choice.object.clone()), w)?;
                    }
                },
                TSTypeDef::Unknown => {
                    w.write_code("unknown")?;
                },
            }
            Ok(())
        }
        write_ts_rec(&self, w)
    }
}

impl Display for TSTypeDef {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        fn fmt_help(
            type_def: &TSTypeDef,
            f: &mut std::fmt::Formatter<'_>,
            depth: usize,
        ) -> std::fmt::Result {
            match type_def {
                TSTypeDef::Primitive(p) => f.write_fmt(format_args!("{}", p)),
                TSTypeDef::Unknown => f.write_str("unknown"),
                TSTypeDef::Object(hash_map) => {
                    let mut key_vals = hash_map.iter();
                    if let Some((first_key, first_val)) = key_vals.next() {
                        f.write_str("{\n")?;
                        for _ in 0..depth + 2 {
                            f.write_str(" ")?;
                        }
                        f.write_fmt(format_args!("{} ", first_key))?;
                        fmt_help(first_val, f, depth + 2);
                        for (key, value) in key_vals {
                            f.write_str(",\n")?;
                            for _ in 0..depth + 2 {
                                f.write_str(" ")?;
                            }
                            f.write_fmt(format_args!("{} ", key))?;
                            fmt_help(value, f, depth + 2);
                        }
                        f.write_char('\n');
                        for _ in 0..depth {
                            f.write_str(" ")?;
                        }
                        f.write_char('}')
                    } else {
                        f.write_str("{}")
                    }
                }
                TSTypeDef::Array(values) => f.write_fmt(format_args!("{}[]", values.as_ref())),
                TSTypeDef::Choice(choice) => {
                    let mut primitives = choice.primitives.iter();
                    if let Some(first) = primitives.next() {
                        f.write_fmt(format_args!("{}", first))?;
                        for choice in primitives {
                            f.write_fmt(format_args!(" | {}", choice))?
                        }
                    }
                    if let Some(array) = &choice.array {
                        if !choice.primitives.is_empty() {
                            f.write_str(" | ")?;
                        }
                        f.write_fmt(format_args!("{}[]", array));
                    }
                    if !choice.object.is_empty() {
                        if choice.array.is_some() || !choice.primitives.is_empty() {
                            f.write_str(" | ")?;
                        }
                        fmt_help(&TSTypeDef::Object(choice.object.clone()), f, depth + 1)?;
                    }
                    Ok(())
                }
            }
        }
        fmt_help(self, f, 0)
    }
}

#[derive(Clone, Debug, Eq, PartialOrd)]
pub struct TSField {
    name: String,
    optional: bool,
}

impl WriteTypescript for TSField {
    fn write_ts<W: std::io::Write>(&self, w: &mut CodeWriter<W>) -> std::io::Result<()> {
        if self.optional {
            w.write_code_key_opt(&self.name)
        } else {
            w.write_code_key(&self.name)
        }
    }
}

struct TSFieldVisitor;

impl<'de> Visitor<'de> for TSFieldVisitor {
    type Value = TSField;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("Expecting a string key")
    }
    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(TSField {
            name: v.to_string(),
            optional: false,
        })
    }
    fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(TSField {
            name: v,
            optional: false,
        })
    }
}

impl<'de> Deserialize<'de> for TSField {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_string(TSFieldVisitor)
    }
}

impl Display for TSField {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.name)?;
        if self.optional {
            f.write_char('?')?;
        };
        f.write_char(':')
    }
}

impl Hash for TSField {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl PartialEq for TSField {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl TSTypeDef {
    pub fn from_serde_yaml(value: &serde_yaml::Value) -> Result<TSTypeDef, String> {
        match value {
            serde_yaml::Value::Null => Ok(Self::Primitive(Primitive::Null)),
            serde_yaml::Value::Bool(_) => Ok(Self::Primitive(Primitive::Boolean)),
            serde_yaml::Value::Number(_) => Ok(Self::Primitive(Primitive::Number)),
            serde_yaml::Value::String(_) => Ok(Self::Primitive(Primitive::String)),
            serde_yaml::Value::Sequence(sequence) => {
                let mut inner = TSTypeDef::Unknown;
                for item in sequence {
                    inner = combine(inner, TSTypeDef::from_serde_yaml(item).unwrap())
                }
                Ok(TSTypeDef::Array(Box::new(inner)))
            }
            serde_yaml::Value::Mapping(mapping) => {
                let mut fields = HashMap::default();

                for (yaml_name, yaml_value) in mapping.iter() {
                    if let serde_yaml::Value::String(ts_name) = yaml_name {
                        let ts_field = TSField {
                            name: ts_name.clone(),
                            optional: false,
                        };

                        if let Ok(ts_value) = Self::from_serde_yaml(yaml_value) {
                            if fields.insert(ts_field, ts_value).is_some() {
                                return Err("Duplicate keys detected.".to_string());
                            }
                        }
                    } else {
                        return Err("Only string keys are supported.".to_string());
                    }
                }

                Ok(Self::Object(fields))
            }
            serde_yaml::Value::Tagged(_) => Err("Tags are not supported.".to_string()),
        }
    }
}

pub fn combine(acc: TSTypeDef, def: TSTypeDef) -> TSTypeDef {
    if acc.eq(&def) {
        return acc;
    }
    match (acc, def) {
        (TSTypeDef::Unknown, TSTypeDef::Unknown) => TSTypeDef::Unknown,
        (TSTypeDef::Unknown, other) | (other, TSTypeDef::Unknown) => other,
        (TSTypeDef::Object(a), TSTypeDef::Object(b)) => TSTypeDef::Object(combine_objects(a, b)),
        (TSTypeDef::Object(object), TSTypeDef::Primitive(p))
        | (TSTypeDef::Primitive(p), TSTypeDef::Object(object)) => {
            let mut primitives = HashSet::default();
            primitives.insert(p);
            TSTypeDef::Choice(Choice {
                object,
                array: None,
                primitives,
            })
        }
        (TSTypeDef::Object(object), TSTypeDef::Array(array))
        | (TSTypeDef::Array(array), TSTypeDef::Object(object)) => TSTypeDef::Choice(Choice {
            object,
            array: Some(array),
            primitives: HashSet::default(),
        }),
        (TSTypeDef::Array(a), TSTypeDef::Array(b)) => {
            let type_def = combine(a.as_ref().clone(), b.as_ref().clone());
            TSTypeDef::Array(Box::new(type_def))
        }
        (TSTypeDef::Primitive(p), TSTypeDef::Array(array))
        | (TSTypeDef::Array(array), TSTypeDef::Primitive(p)) => {
            let mut primitives = HashSet::default();
            primitives.insert(p);
            TSTypeDef::Choice(Choice {
                array: Some(array),
                object: HashMap::default(),
                primitives,
            })
        }
        (TSTypeDef::Choice(mut inner_a), TSTypeDef::Choice(mut inner_b)) => {
            let array = match (inner_a.array.take(), inner_b.array.take()) {
                (None, None) => None,
                (None, Some(inner)) | (Some(inner), None) => Some(inner),
                (Some(a), Some(b)) => Some(Box::new(combine(*a, *b))),
            };
            let object = combine_objects(inner_a.object, inner_b.object);
            let mut primitives = HashSet::default();

            for item in inner_a.primitives {
                primitives.insert(item);
            }
            for item in inner_b.primitives {
                primitives.insert(item);
            }
            TSTypeDef::Choice(Choice {
                array,
                object,
                primitives,
            })
        }
        (TSTypeDef::Choice(mut choice), TSTypeDef::Array(any))
        | (TSTypeDef::Array(any), TSTypeDef::Choice(mut choice)) => {
            let array = match choice.array.take() {
                None => any,
                Some(inner) => Box::new(combine(*inner, *any)),
            };
            TSTypeDef::Choice(Choice {
                object: choice.object,
                primitives: choice.primitives,
                array: Some(array),
            })
        }
        (TSTypeDef::Choice(choice), TSTypeDef::Object(object))
        | (TSTypeDef::Object(object), TSTypeDef::Choice(choice)) => TSTypeDef::Choice(Choice {
            array: choice.array,
            primitives: choice.primitives,
            object: combine_objects(choice.object, object),
        }),
        (TSTypeDef::Choice(mut choice), TSTypeDef::Primitive(any))
        | (TSTypeDef::Primitive(any), TSTypeDef::Choice(mut choice)) => {
            choice.primitives.insert(any);
            TSTypeDef::Choice(choice)
        }
        (TSTypeDef::Primitive(acc), TSTypeDef::Primitive(def)) => {
            if acc.eq(&def) {
                TSTypeDef::Primitive(acc)
            } else {
                let mut set = HashSet::default();
                set.insert(acc);
                set.insert(def);
                TSTypeDef::Choice(Choice {
                    array: None,
                    object: HashMap::default(),
                    primitives: set,
                })
            }
        }
    }
}

fn combine_objects(
    a: HashMap<TSField, TSTypeDef>,
    b: HashMap<TSField, TSTypeDef>,
) -> HashMap<TSField, TSTypeDef> {
    let mut fields = HashMap::default();

    for (field_a, type_def_a) in a.iter() {
        match b.get_key_value(field_a) {
            None => {
                let field = TSField {
                    optional: true,
                    ..field_a.clone()
                };
                fields.insert(field, type_def_a.clone());
            }
            Some((field_b, type_def_b)) => {
                let field = TSField {
                    optional: field_a.optional || field_b.optional,
                    name: field_a.name.clone(),
                };

                let type_def = combine(type_def_a.clone(), type_def_b.clone());
                fields.insert(field, type_def);
            }
        }
    }
    for (field_b, type_def_b) in b.iter() {
        match a.get_key_value(field_b) {
            None => {
                let field = TSField {
                    optional: true,
                    ..field_b.clone()
                };
                fields.insert(field, type_def_b.clone());
            }
            Some((field_a, type_def_a)) => {
                let field = TSField {
                    optional: field_b.optional || field_a.optional,
                    name: field_b.name.clone(),
                };

                let type_def = combine(type_def_b.clone(), type_def_a.clone());
                fields.insert(field, type_def);
            }
        }
    }

    fields
}

#[cfg(test)]
mod test {
    use super::*;
    use proptest::prelude::*;
    fn yaml_leaves() -> impl Strategy<Value = serde_yaml::Value> {
        prop_oneof![
            Just(serde_yaml::Value::Null),
            Just(serde_yaml::Value::Bool(true)),
            Just(serde_yaml::Value::Bool(false)),
            ".*".prop_map(serde_yaml::Value::String)
        ]
    }
    fn any_yaml() -> impl Strategy<Value = serde_yaml::Value> {
        yaml_leaves().prop_recursive(8, 256, 10, |inner| {
            prop_oneof![
                prop::collection::vec(inner.clone(), 0..10).prop_map(serde_yaml::Value::Sequence),
                prop::collection::hash_map(".*".prop_map(serde_yaml::Value::String), inner, 0..10)
                    .prop_map(serde_yaml::Mapping::from_iter)
                    .prop_map(serde_yaml::Value::Mapping)
            ]
        })
    }
    fn yaml_mapping() -> impl Strategy<Value = serde_yaml::Value> {
        prop::collection::hash_map(".*".prop_map(serde_yaml::Value::String), any_yaml(), 0..10)
            .prop_map(serde_yaml::Mapping::from_iter)
            .prop_map(serde_yaml::Value::Mapping)
    }

    proptest! {
        #[test]
        fn can_get_types_for_any_yaml_mapping(value in yaml_mapping()) {
            let inferrence = TSTypeDef::from_serde_yaml(&value);
            prop_assert!(inferrence.is_ok())
        }
        #[test]
        fn ts_objs_are_identity_for_themselves(value in yaml_mapping()) {
            let inferrence = TSTypeDef::from_serde_yaml(&value).unwrap();
            prop_assert!(combine(inferrence.clone(), inferrence.clone()) == inferrence);
        }
        #[test]
        fn empty_obj_is_r_identity_for_ts_obj(value in yaml_mapping()) {
            let inferrence = TSTypeDef::from_serde_yaml(&value).unwrap();
            let empty = TSTypeDef::Object(HashMap::default());
            prop_assert!(combine(inferrence.clone(), empty) == inferrence)
        }
        #[test]
        fn empty_obj_is_l_identity_for_ts_obj(value in yaml_mapping()) {
            let inferrence = TSTypeDef::from_serde_yaml(&value).unwrap();
            let empty = TSTypeDef::Object(HashMap::default());
            prop_assert!(combine(empty, inferrence.clone()) == inferrence)
        }
        #[test]
        fn combine_is_associative_for_ts_obj(a in yaml_mapping(), b in yaml_mapping(), c in yaml_mapping()) {
            let inferrence_a = TSTypeDef::from_serde_yaml(&a).unwrap();
            let inferrence_b = TSTypeDef::from_serde_yaml(&b).unwrap();
            let inferrence_c = TSTypeDef::from_serde_yaml(&c).unwrap();
            let l = combine(inferrence_a.clone(), combine(inferrence_b.clone(), inferrence_c.clone()));
            let r = combine(combine(inferrence_a, inferrence_b), inferrence_c);

            prop_assert!(l == r)
        }
        #[test]
        fn unknown_is_l_identity_for_ts_types(a in any_yaml()) {
            let inferrence = TSTypeDef::from_serde_yaml(&a).unwrap();
            prop_assert!(inferrence.clone() == combine(TSTypeDef::Unknown, inferrence))
        }
        #[test]
        fn unknown_is_r_identity_for_ts_types(a in any_yaml()) {
            let inferrence = TSTypeDef::from_serde_yaml(&a).unwrap();
            prop_assert!(inferrence.clone() == combine(inferrence, TSTypeDef::Unknown))
        }
    }
}
