use std::io::Write;

pub fn write_pascal_case<W: Write, I: Iterator<Item = char>>(
    w: &mut W,
    chars: &mut I,
) -> std::io::Result<()> {
    if let Some(next) = chars.next() {
        w.write_all(&[next.to_ascii_uppercase() as u8])?;
        write_camel_case(w, chars)?;
    }
    Ok(())
}

pub fn write_camel_case<W: Write, I: Iterator<Item = char>>(
    w: &mut W,
    chars: I,
) -> std::io::Result<()> {
    let mut cap = false;
    for c in chars {
        if c.is_ascii_punctuation() || c.is_ascii_whitespace() {
            cap = true;
            continue;
        }
        let c = if cap {
            c.to_ascii_uppercase()
        } else {
            c.to_ascii_lowercase()
        };
        w.write_all(&[c as u8])?;
        cap = false;
    }
    Ok(())
}

pub fn write_kebab_case<W: Write, I: Iterator<Item = char>>(
    w: &mut W,
    chars: I,
) -> std::io::Result<()> {
    for c in chars {
        if c.is_ascii_alphabetic() || c.is_ascii_digit() {
            w.write_all(&[c.to_ascii_lowercase() as u8])?;
        }
        if c.is_ascii_whitespace() {
            w.write_all(b"-")?;
            continue;
        }
    }
    Ok(())
}
pub fn to_kebab_case(
    str: &str,
) -> String {
    let mut chars = str.chars();
    let mut w = Vec::default();
    write_kebab_case(&mut w, &mut chars);
    String::from_utf8(w).unwrap()
}

pub fn to_pascal_case(
    str: &str,
) -> String {
    let mut chars = str.chars();
    let mut w = Vec::default();
    write_pascal_case(&mut w, &mut chars);
    String::from_utf8(w).unwrap()
}

pub fn to_camel_case(
    str: &str,
) -> String {
    let chars = str.chars(); 
    let mut w = Vec::default();
    write_camel_case(&mut w, chars);
    String::from_utf8(w).unwrap()
}