use std::{
    collections::HashMap,
    io::{BufWriter, Write},
    path::Path,
    sync::{Arc, Mutex},
};

use crate::{
    config::Config,
    err,
    fmt::to_pascal_case,
    stats::Stats,
    store::ContentStore,
    type_definitions::TypeGenerator, codegen::CodeWriter,
};

/*
  Generates a taxonomies file based on content paths.
*/

pub fn generate(
    store: Arc<ContentStore>,
    config: Arc<Config>,
    stats: Arc<Mutex<Stats>>,
    type_gen: Arc<Mutex<TypeGenerator>>,
) {
    let empty_path: &'static Path = Path::new("");
    if store.is_empty() {
        return;
    }
    let now = std::time::Instant::now();
    let outdir: &Path = config.output_folder.as_ref();

    let mut map: HashMap<&Path, Vec<usize>> = HashMap::new();
    for (idx, path_r) in store.paths().enumerate() {
        let path: &Path = store.read(*path_r).as_ref();
        let path = path.strip_prefix(&config.content_folder).unwrap_or(path);

        let mut ancestors = path.ancestors();
        // The first result is the entire path and file name.
        ancestors.next();
        for segment in ancestors.filter(|p| p != &empty_path) {
            if let Some(entry) = map.get_mut(&segment) {
                entry.push(idx);
            } else {
                map.insert(segment, vec![idx]);
            }
        }
    }
    let result = write_collections(outdir, &map);

    let mut stats = stats.lock().unwrap();
    stats.taxonomies_elapsed = now.elapsed().as_millis();
    stats.total_taxonomies = map.len();
    let gen = type_gen.lock().unwrap();
    for (key, value) in map {
        gen.generate(format!("{}", key.display()), value);
    }
    if let Err(e) = result {
        stats.errors.push(e);
    }
}

fn write_collections<P: AsRef<Path>>(
    outdir: P,
    map: &HashMap<&Path, Vec<usize>>,
) -> Result<(), err::Error> {
    let file_path = std::fs::File::create(outdir.as_ref().join("taxonomies.tsx")).unwrap();
    let file = BufWriter::new(file_path);
    let mut w = CodeWriter::new(file, None);
    write_imports(&mut w, map)?;
    w.write_code("import * as C from \"./content\";\n\n")?;
    for (tag, values) in map {
        write_collection(&mut w, tag, values.iter())?;
    }
    Ok(())
}

fn write_collection<'a, W: Write, I: Iterator<Item = &'a usize>>(
    w: &mut CodeWriter<W>,
    tag: &Path,
    values: I,
) -> Result<(), err::Error> {
    w.write_code("export const ")?;
    w.write_camel_case(tag.to_string_lossy().chars())?;
    w.write_code(": ")?;
    w.write_pascal_case(&mut tag.to_string_lossy().chars())?;
    w.write_code("[] = ")?;
    w.start_list()?;
    for value in values.map(|s| format!("C.q{}", s)) {
        w.start_list_item()?;
        w.write_code(&value)?;
        w.end_list_item()?;
    }
    w.end_list()?;
    w.write_code(";\n\n")?;
    Ok(())
}

fn write_imports<W: Write>(w: &mut CodeWriter<W>, map: &HashMap<&Path, Vec<usize>>) -> Result<(), err::Error> {
    let mut imports = map.keys().map(|s| format!("type {}", to_pascal_case(s.to_str().unwrap())));
    w.write_import(&mut imports, "./types")?;
    Ok(())
}
