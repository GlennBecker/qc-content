use std::{
    collections::HashMap,
    fmt::Display,
    io::{BufWriter, Write},
    path::Path,
    sync::{Arc, Mutex},
};

use crate::{
    config::Config,
    err,
    fmt::to_pascal_case,
    stats::Stats,
    store::ContentStore,
    type_definitions::TypeGenerator, codegen::CodeWriter,
};

/*
  Creates a collections file based on the presence of tags in the fronmatter.
*/

#[derive(serde::Deserialize, Debug)]
struct Data<'a> {
    #[serde(borrow, default)]
    tags: Vec<&'a str>,
}

pub fn generate(
    store: Arc<ContentStore>,
    config: Arc<Config>,
    stats: Arc<Mutex<Stats>>,
    type_gen: Arc<Mutex<TypeGenerator>>,
) {
    if store.is_empty() {
        return;
    }
    let now = std::time::Instant::now();
    let mut map: HashMap<&str, Vec<usize>> = HashMap::default();
    for (idx, frontmatter_r) in store.frontmatter().enumerate() {
        let str = store.read(*frontmatter_r).trim().trim_matches(|c| c == '-');
        let data = serde_yaml::from_str::<Data>(str).unwrap();
        for tag in data.tags {
            if let Some(entry) = map.get_mut(&tag) {
                entry.push(idx);
            } else {
                map.insert(tag, vec![idx]);
            }
        }
    }
    map.insert("all", (0..store.len()).into_iter().collect());
    let result = write_collections(&config.output_folder, &map);
    let mut stats = stats.lock().unwrap();
    stats.collections_elapsed = now.elapsed().as_millis();
    stats.total_collections = map.len();
    let gen = type_gen.lock().unwrap();
    for (key, value) in map {
        gen.generate(key.to_owned(), value);
    }
    gen.generate("All".to_string(), (0..store.len()).into_iter().collect());
    if let Err(e) = result {
        stats.errors.push(e);
    }
}

fn write_imports<W: Write>(
    w: &mut CodeWriter<W>,
    map: &HashMap<&str, Vec<usize>>,
) -> Result<(), err::Error> {
    let mut imports = map.keys()
        .map(|s| format!("type {}", to_pascal_case(s)))
    ;
    w.write_import(&mut imports, "./types")?;
    Ok(())
}

pub fn write_collections<P: AsRef<Path>>(
    outdir: P,
    map: &HashMap<&str, Vec<usize>>,
) -> Result<(), err::Error> {
    let file_path = std::fs::File::create(outdir.as_ref().join("collections.tsx")).unwrap();
    let file = BufWriter::new(file_path);
    let mut w = CodeWriter::new(file, None);
    write_imports(&mut w, map)?;
    w.write_code("import * as C from \"./content\"\n\n")?;
    for (tag, values) in map {
        let values = values.iter();
        write_collection(&mut w, tag, values)?;
    }
    w.flush()?;
    Ok(())
}

pub fn write_collection<W: Write, I>(w: &mut CodeWriter<W>, tag: &str, values: I) -> Result<(), err::Error>
where
    I: Iterator,
    I::Item: Display,
{
    let fmtted = values.map(|s| format!("C.q{}", s));
    w.write_code("export const ")?;
    w.write_camel_case(tag.chars())?;
    w.write_code(": ")?;
    w.write_pascal_case(&mut tag.chars())?;
    w.write_code("[] = ")?;
    w.start_list()?;
    
    for value in fmtted {
        w.start_list_item()?;
        w.write_code(&value)?;
        w.end_list_item()?;
    }

    w.end_list()?;
    w.write_code(";")?;
    w.end_line()?;
    w.end_line()?;
    Ok(())
}
