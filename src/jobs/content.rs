use crate::{mdx::Content, codegen::{WriteJavascript, CodeWriter}};
use std::{
    collections::hash_map::DefaultHasher,
    hash::Hasher,
    io::{BufWriter, Write},
    path::Path,
    sync::{Arc, Mutex},
};

use crate::mdx::MDXParser;
use crate::{config::Config, err, stats::Stats, store::ContentStore};

struct Page<'a> {
    _id: String,
    _path: Option<&'a Path>,
    _slug: &'a str,
    _extension: &'a str,
    _raw: &'a str,
    _content: Vec<Content>,
    _headings: Vec<(String, String)>,
    frontmatter: serde_yaml::Mapping,
}

/*
  Processes all content files and writes them to the configured file directory.

  Generated file exports a JSON object and contains any imports if the file is mdx.
*/

pub fn generate(store: Arc<ContentStore>, config: Arc<Config>, stats: Arc<Mutex<Stats>>) {
    if store.is_empty() {
        return;
    }
    let now = std::time::Instant::now();
    let outdir: &Path = config.output_folder.as_ref();
    let file = std::fs::File::create(outdir.join("content.tsx")).unwrap();
    let mut w = CodeWriter::new(BufWriter::new(file), None);

    let mut imports = store.imports();
    if let Some(next) = imports.next() {
        let lines = store
            .read(*next)
            .trim()
            .lines()
            .filter(|s| !s.trim().is_empty());
        for line in lines {
            w.write_code(&format!("{}\n", line.trim()));
        }
        for import in imports {
            let lines = store
                .read(*import)
                .trim()
                .lines()
                .filter(|s| !s.trim().is_empty());
            for line in lines {
                w.write_code(&format!("{}\n", line.trim()));
            }
        }
        w.end_line();
    }
    let options = config.to_pulldown_opts();
    let pages = store
        .paths()
        .zip(store.frontmatter())
        .zip(store.bodies())
        .map(|((path, frontmatter), bodies)| {
            (
                store.read(*path),
                store.read(*frontmatter),
                store.read(*bodies),
            )
        });
    for (idx, (full_path, frontmatter, body)) in pages.enumerate() {        
        let mut hasher = DefaultHasher::new();
        hasher.write(full_path.as_bytes());
        let id = hasher.finish().to_string();
        let path: &Path = full_path.strip_prefix(&config.content_folder).unwrap().as_ref();
        let directory = path.parent();
        let slug = path.file_stem().and_then(|s| s.to_str()).unwrap();
        let extension = path.extension().and_then(|s| s.to_str()).unwrap();
        let frontmatter = frontmatter.trim_end_matches(|c| c == '-');
        let mut frontmatter: serde_yaml::Mapping = 
            serde_yaml::from_str(frontmatter).unwrap_or_default();

        for segment in Path::new(full_path).ancestors() {
            println!("{}", segment.display());
            if let Some(dir_frontmatter) = store.config.get(segment) {
                println!("Config available for {}", segment.display());
                merge_frontmatter_mapping(&mut frontmatter, &dir_frontmatter);
            }
        }
        let (content, headings) = MDXParser::new(pulldown_cmark::Parser::new_ext(body, options)).get_content().unwrap();
        match write_page(
            &mut w,
            idx,
            Page {
                _id: id,
                _path: directory,
                _slug: slug,
                _extension: extension,
                _content: content,
                _headings: headings,
                frontmatter,
                _raw: body,
            },
        ) {
            Ok(_) => {
                let mut stats = stats.lock().unwrap();
                stats.total_files += 1;
            }
            Err(e) => {
                let mut stats = stats.lock().unwrap();
                stats.errors.push(e);
            }
        };
    }
    let mut stats = stats.lock().unwrap();
    if let Err(e) = w.flush() {
        stats.errors.push(e.into())
    }
    stats.content_elapsed = now.elapsed().as_millis();
}

fn write_page<W: Write>(w: &mut CodeWriter<W>, idx: usize, page: Page) -> Result<(), err::Error> {
    w.write_code(&format!("export const q{} = ", idx))?;
    page.write_js(w)?;
    // serde_json::to_writer_pretty(w.by_ref(), &page)?;
    w.end_line()?;
    w.end_line()?;
    Ok(())
}

impl WriteJavascript for serde_yaml::Mapping {
    fn write_js<W: std::io::Write>(&self, w: &mut CodeWriter<W>) -> Result<(), std::io::Error> {
        fn write_js_rec<W: std::io::Write>(depth: usize, w: &mut CodeWriter<W>, value: &serde_yaml::Value) -> Result<(), std::io::Error> {
            match value {
                serde_yaml::Value::Null => {
                    w.write_code("null")?;
                },
                serde_yaml::Value::Bool(bool) => {
                    w.write_bool(*bool)?;
                },
                serde_yaml::Value::Number(number) => {
                    w.write_code(&format!("{}", number))?;
                },
                serde_yaml::Value::String(string) => {
                    w.write_str(string)?;
                },
                serde_yaml::Value::Sequence(xs) => {
                    w.start_list()?;
                    for value in xs {
                        w.start_list_item()?;
                        write_js_rec(depth, w, value)?;
                        w.end_list_item()?;
                    }
                    w.end_list()?;
                },
                serde_yaml::Value::Mapping(mapping) => {
                    write_mapping(depth, w, mapping)?;
                },
                serde_yaml::Value::Tagged(_) => todo!(),
            }
            Ok(())
        }
        fn write_mapping<W: std::io::Write>(depth: usize, w: &mut CodeWriter<W>, mapping: &serde_yaml::Mapping) -> Result<(), std::io::Error> {
            w.start_obj()?;
            let iter = mapping.into_iter()
            .filter_map(|(k,v)| match k {
                serde_yaml::Value::String(s) => {
                    Some((s, v))
                }
                _ => None
            });
            for (key, value) in iter {
                w.write_key(&key)?;
                write_js_rec(depth, w, value)?;
                w.end_key_value()?;
            }
            w.end_obj()?;
            Ok(())
        }
        if self.is_empty() {
            return Ok(())
        }
        let iter = self.iter().filter_map(|(k, v)| match k {
            serde_yaml::Value::String(s) => Some((s, v)),
            _ => None
        });
        for (key, value) in iter {
            w.write_key(key)?;
            write_js_rec(1, w, value)?;
            w.end_key_value()?;
        }
        Ok(())
    }
}

impl<'a> WriteJavascript for Page<'a> {
    fn write_js<W: std::io::Write>(&self, w: &mut CodeWriter<W>) -> std::io::Result<()> {
        fn write_str_kv<W: std::io::Write>(w: &mut CodeWriter<W>, key: &str, val: &str) -> std::io::Result<()> {
            w.write_code_key(key)?;
            w.write_str(val)?;
            w.end_key_value()
        }
        w.start_obj()?;

        write_str_kv(w, "_id", &format!("{}", self._id))?;
        write_str_kv(w, "_path", self._path.and_then(|p| p.to_str()).unwrap_or("/"))?;
        write_str_kv(w, "_slug", self._slug)?;
        write_str_kv(w, "_extension", self._extension)?;
        
        w.write_code_key("_raw")?;
        w.write_str_literal(self._raw.trim())?;
        w.end_key_value()?;
        
        w.write_code_key("_content")?;
        w.start_list()?;
        for item in self._content.iter() {
            w.start_list_item()?;
            match item {
                Content::Markdown(md) => {
                    w.write_str_literal(&md)?;
                }
                Content::Jsx(vec) => {
                    let mut jsx = vec.iter();
                    if let Some(first) = jsx.next() {
                        w.write_code(&format!("    {}", first.trim_end()))?;
                        for jsx in jsx {
                            w.write_code(&format!("\n    {}", jsx.trim_end()))?;
                        }
                    }
                }
            }
            w.end_list_item()?;
        }
        w.end_list()?;
        w.end_key_value()?;

        w.write_code_key("_headings")?;
        w.start_list()?;
        for (key, value) in self._headings.iter() {
            w.start_list_item()?;
            w.write_code(&format!("{{ id: {:?}, text: {:?} }}", key, value))?;
            w.end_list_item()?;
        }
        w.end_list()?;
        w.end_key_value()?;

        self.frontmatter.write_js(w)?;
        
        w.end_obj()?;
        w.write_code(";")?;
        w.flush()?;
        Ok(())
    }
}

pub fn merge_frontmatter_mapping(a: &mut serde_yaml::Mapping, b: &serde_yaml::Mapping) {
    for (key, value) in b.into_iter() {
        match a.get_mut(key) {
            Some(a_val) => {
                println!("Merging {:?} {:?}", a_val, value);
                merge_frontmatter(a_val, value)
            }
            None => {
                println!("Inserting {:?}: {:?}", key, value);
                a.insert(key.clone(), value.clone());
            }
        }
    }    
}

pub fn merge_frontmatter(a: &mut serde_yaml::Value, b: &serde_yaml::Value)  {
    use serde_yaml::Value::*;
    println!("Can't merge {:?} with {:?}", a, b);
    match (a, b) {
        (Sequence(xs), Sequence(ys)) => {
            for value in ys.iter() {
                if !xs.contains(value) {
                    xs.push(value.clone());
                }
            }
        }
        (Mapping(a), Mapping(b)) => {
            for (key, value) in b.into_iter() {
                match a.get_mut(key) {
                    Some(a_val) => {
                        merge_frontmatter(a_val, value)
                    }
                    None => {
                        a.insert(key.clone(), value.clone());
                    }
                }
            }
        }
        (_, _) => {}
    }
}