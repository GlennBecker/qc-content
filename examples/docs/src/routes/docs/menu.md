# Guide

## Examples

- [Hello World](https://qwik.builder.io/examples/introduction/hello-world/)
- [Tutorials](https://qwik.builder.io/tutorial/welcome/overview/)
- [Playground](https://qwik.builder.io/playground/)

## Community

- [@QwikDev](https://twitter.com/QwikDev)
- [Discord](https://qwik.builder.io/chat)
- [Github](https://github.com/BuilderIO/qwik)
