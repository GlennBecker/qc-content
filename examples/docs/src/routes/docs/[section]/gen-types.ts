export interface RouteParams extends Record<string, string | undefined> {
  "section": string
}