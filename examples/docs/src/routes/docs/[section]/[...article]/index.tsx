import { component$ } from "@builder.io/qwik";
import { useLocation } from "@builder.io/qwik-city";
import { RouteParams } from "./gen-types";

export default component$(() => {
  const { article, section } = useLocation().params as RouteParams;
  return <><p>{article}</p>
    <p>{section || "root"}</p>
  </>
})