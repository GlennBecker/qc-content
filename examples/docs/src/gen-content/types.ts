import { QwikJSX } from "@builder.io/qwik/";

export type ContentArray = (QwikJSX.Element | string)[];

export interface Page {
  _id: string,
  _path: string,
  _slug: string,
  _extension: string,
  _raw: string,
  _content: ContentArray,
  _headings: {id: string, text: string}[]
}

export interface DocsMore extends Page {
  menu: {
    show_main: boolean
  },
  tags: string[],
  title: string
}

export interface Docs extends Page {
  draft?: boolean,
  title: string,
  menu?: {
    weight?: number,
    key?: string,
    show_main?: boolean
  },
  description?: string,
  mystery?: unknown[],
  tags: string[]
}

export interface All extends Page {
  tags?: string[],
  title: string | number,
  draft?: boolean,
  menu?: {
    show_main?: boolean,
    id?: string,
    key?: string,
    weight?: number
  },
  description?: string,
  mystery?: unknown[]
}

export interface QwikCity extends Page {
  menu: {
    show_main: boolean
  },
  tags: string[],
  title: string
}

export interface HttpGoogleCom extends Page {
  title: string,
  tags: string[]
}

export interface FunTimes extends Page {
  tags: string[],
  title: string,
  menu: {
    show_main: boolean
  }
}

export interface Qwik extends Page {
  menu: {
    key?: string,
    weight?: number,
    show_main?: boolean
  },
  description?: string,
  title: string,
  tags: string[]
}

export interface Beginner extends Page {
  tags: string[],
  title: string,
  menu: {
    key?: string,
    show_main?: boolean,
    weight?: number
  },
  description?: string
}

export interface Advanced extends Page {
  mystery: unknown[],
  tags: string[],
  title: string,
  draft: boolean
}

export interface All extends Page {
  title: string | number,
  tags?: string[],
  mystery?: unknown[],
  description?: string,
  draft?: boolean,
  menu?: {
    weight?: number,
    show_main?: boolean,
    id?: string,
    key?: string
  }
}

